#!/bin/bash

{ cat snaps_from_canonical.list; echo; } | while read -r PKG; do
	rm -rf snaps_from_canonical
	mkdir -p snaps_from_canonical
	cd snaps_from_canonical
	echo "Downloading $PKG..."
	snap download $PKG &>/dev/null
	rm -f *.assert
	filename="$(echo *.snap)"
	version="$(snap info $filename | grep -i '^version\:' | awk '{print $2}')"
	mv "$filename" ..; cd ..
	python3 ./add-snap.py "$filename" "$PKG" \
		"$(dpkg --print-architecture)" "$version"
	rm -f "$filename"
done

rm -rf snaps_from_canonical