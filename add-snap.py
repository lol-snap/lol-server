#!/usr/bin/env python3

import os, sys
import argparse
import json
import hashlib

# Create the parser
parser = argparse.ArgumentParser(description='Add snap to store/server')

# Add the arguments
parser.add_argument('snap',
                       metavar='snap',
                       type=str,
                       help='Path to snap')
parser.add_argument('name',
                       metavar='name',
                       type=str,
                       help='Snap name')
parser.add_argument('arch',
                       metavar='arch',
                       type=str,
                       help='Snap architecture')
parser.add_argument('version',
                       metavar='version',
                       type=str,
                       help='Snap version')

args = parser.parse_args()

snap = args.snap
name = args.name
arch = args.arch
version = args.version

snap_dir = 'server/snap/v1/' + name

if not os.path.isfile(snap):
    print('The snap file does not exist')
    sys.exit()

snap_file = snap_dir + '/' + name + '_' + version + '_' + arch + '.snap'

os.system('rm -rf ' + snap_dir)
os.system('mkdir -p ' + snap_dir)
os.system('cp ' + snap + ' ' + snap_file)

json_data = {
    'name': name,
    'version': version,
    'arch': arch,
    'md5sum': hashlib.md5(open(snap_file, 'rb').read()).hexdigest()
}

with open(snap_dir + '/' + name + '.json', 'w') as jsonFile:
    json.dump(json_data, jsonFile)