# lol server

Sample server for `lol`, a wrapper around `snap` with support for other snap servers.

# Dependencies

The only and only `python3` ;)

# Running

`python3 -m http.server --directory server 8080` after cloning this repo.

# Adding snaps

Run `python3 ./add-snap.py <PATH_TO_SNAP> <SNAP_NAME> <SNAP_ARCH> <SNAP_VERSION>`

# Notes

This server is not supposed to be used in production, although we are nearing the `reprepro` level.
